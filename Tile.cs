using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Platformia;

public enum TileState
{
    Held,
    Released
}

public class Tile
{
    public Vector2 center;
    public string name;
    public Vector2 position;
    public Vector2 size;
    public Texture2D sprite;
    public TileState state = TileState.Released;

    public Tile(Vector2 newPos, Vector2 newSize, string spriteName, ContentManager contentManager)
    {
        position = newPos;
        size = newSize;
        name = spriteName;
        sprite = contentManager.Load<Texture2D>(name);
        center = new Vector2(position.X - size.X / 2, position.Y - size.Y / 2);
    }

    public void Update(GameTime gameTime)
    {
        center = new Vector2(position.X - size.X / 2, position.Y - size.Y / 2);
    }

    public void Draw(GameTime gameTime, SpriteBatch _spriteBatch)
    {
        _spriteBatch.Draw(sprite, center, Color.White);
    }
}