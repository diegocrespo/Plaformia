﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Platformia;

public class Game1 : Game
{
    private readonly GraphicsDeviceManager _graphics;
    private readonly bool debug = true;
    private readonly int screenHeight = 800;
    private readonly int screenWidth = 1280;
    private readonly Vector2 tileSize = new(70, 70);
    private SpriteBatch _spriteBatch;
    private Hud hud;
    private SpriteFont kenFont;
    private Shape line;
    private MouseState mState;
    private Texture2D playerSprite;
    private Shape rectangle;
    private Tile tile;
    private List<Tile> Tiles = new();
    private Texture2D tileSprite;
    private double timer = 0.5;

    public Game1()
    {
        _graphics = new GraphicsDeviceManager(this);
        Content.RootDirectory = "Content";
        IsMouseVisible = true;
    }

    protected override void Initialize()
    {
        _graphics.PreferredBackBufferWidth = screenWidth;
        _graphics.PreferredBackBufferHeight = screenHeight;
        _graphics.ApplyChanges();

        base.Initialize();
    }

    protected override void LoadContent()
    {
        _spriteBatch = new SpriteBatch(GraphicsDevice);
        tileSprite = Content.Load<Texture2D>("Tiles/grassMid");
        kenFont = Content.Load<SpriteFont>("Fonts/kenvector_future");
        rectangle = new Shape(_spriteBatch, new Rectangle(100, 100, 250, 150));
        line = new Shape(_spriteBatch, new Vector2(100, 0), new Vector2(100, 600), 1.0f, Color.Red);
        tile = new Tile(new Vector2(0 + tileSize.X / 2, 0 + tileSize.Y / 2), tileSize, "Tiles/grassMid", Content);
        var hudTiles = new List<Tile> { tile };

        hud = new Hud(Content);
        foreach (var t in hudTiles) hud.hudTiles.Add(t);
    }

    protected override void Update(GameTime gameTime)
    {
        if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed ||
            Keyboard.GetState().IsKeyDown(Keys.Escape))
            Exit();

        mState = Mouse.GetState();
        hud.Update(gameTime, mState);
        base.Update(gameTime);
    }

    protected override void Draw(GameTime gameTime)
    {
        GraphicsDevice.Clear(Color.CornflowerBlue);
        _spriteBatch.Begin();
        //rectangle.DrawRectangle();
        rectangle.DrawUnfilledRectangle();
        //line.DrawLine();
        hud.Draw(gameTime, _spriteBatch);
        //if (timer <= 0)
        //{
        //    timer = 0.5;
        //}
        //else
        //{
        //    timer -= gameTime.ElapsedGameTime.TotalSeconds;
        //} 

        if (debug)
        {
            var mPos = mState.Position.ToString();
            var mSize = kenFont.MeasureString(mPos);
            _spriteBatch.DrawString(kenFont, mState.Position.ToString(), new Vector2(screenWidth - mSize.X, 0),
                Color.White);
        }

        _spriteBatch.End();


        base.Draw(gameTime);
    }
}