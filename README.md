# Introduction
Platformia is a game about a young alien. You are a Space Cadet in training and are being put through a series of
grueling platforming levels until you have the necessary skills to go out into the hostile void of Space.