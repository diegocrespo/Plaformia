using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Platformia;

public class Hud
{
    public ContentManager contentManager;
    public List<Tile> gameTiles = new();
    public List<Tile> hudTiles = new();
    public MouseState prevMouseState;

    public Hud(ContentManager content)
    {
        contentManager = content;
    }

    public void Update(GameTime gameTime, MouseState mState)
    {
        var mousePos = mState.Position.ToVector2();
        foreach (var t in hudTiles)
        {
            // Left Click Select
            if (Vector2.Distance(mousePos, t.position) < t.size.X && mState.LeftButton == ButtonState.Pressed
                                                                  && t.state == TileState.Released
                                                                  && prevMouseState.LeftButton == ButtonState.Released)
                t.state = TileState.Held;
            //var newTile = new Tile(mousePos, new Vector2(70, 70), "Tiles/grassMid", contentManager);
            //newTile.state = TileState.Held;
            //gameTiles.Add(newTile);
            // Left Click Deselect
            else if (Vector2.Distance(mousePos, t.position) < t.size.X && mState.LeftButton == ButtonState.Pressed
                                                                       && t.state == TileState.Held
                                                                       && prevMouseState.LeftButton ==
                                                                       ButtonState.Released)
            {
                t.state = TileState.Released;
            }
            t.Update(gameTime);
        }

        //foreach (var t in gameTiles)
        //{
        //    if (t.state == TileState.Held) t.position = mousePos;
        //    if (mState.LeftButton == ButtonState.Released) t.state = TileState.Released;
        //    t.Update(gameTime);
        //}

        prevMouseState = mState;
    }

    public void Draw(GameTime gameTime, SpriteBatch _spriteBatch)
    {
        foreach (var t in hudTiles)
        {
            t.Draw(gameTime, _spriteBatch);
            if (t.state == TileState.Held)
            {
                var tileSize = (int)t.size.X;
                var highlight = new Shape(_spriteBatch, new Rectangle((int)t.center.X, (int)t.center.Y,
                    tileSize, tileSize));
                highlight.DrawUnfilledRectangle();
            }
        }

        foreach (var t in gameTiles) t.Draw(gameTime, _spriteBatch);
    }
}