using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Platformia;

public class Shape
{
    private readonly SpriteBatch _spriteBatch;
    public float angle;
    public Color color = Color.White;
    public Vector2 cord1;
    public Vector2 cord2;
    public float dist;
    public Rectangle rectangle;
    public Vector2 scale;
    public Texture2D tex;
    public float thickness = 1f;

    public Shape(SpriteBatch _spriteBatch, Rectangle rec, float thickness = 1f)
    {
        this._spriteBatch = _spriteBatch;
        rectangle = rec;
        tex = new Texture2D(_spriteBatch.GraphicsDevice, 1, 1);
        tex.SetData(new[] { Color.White });
    }

    public Shape(SpriteBatch _spriteBatch, Vector2 cord1, Vector2 cord2, float thickness)
    {
        this._spriteBatch = _spriteBatch;
        this.cord1 = cord1;
        this.cord2 = cord2;
        this.thickness = thickness;

        dist = Vector2.Distance(this.cord1, this.cord2);
        angle = (float)Math.Atan2(this.cord2.Y - this.cord1.Y, this.cord2.X - this.cord1.X);
        scale = new Vector2(dist, this.thickness);
        tex = new Texture2D(_spriteBatch.GraphicsDevice, 1, 1);
        tex.SetData(new[] { color });
    }

    public Shape(SpriteBatch _spriteBatch, Vector2 cord1, Vector2 cord2, float thickness, Color color)
        : this(_spriteBatch, cord1, cord2, thickness) // Call the other constructor
    {
        this.color = color;
    }

    public void DrawRectangle()
    {
        _spriteBatch.Draw(tex, rectangle, color);
    }

    public void DrawUnfilledRectangle()
    {
        cord1 = new Vector2(rectangle.X, rectangle.Y);
        cord2 = new Vector2(rectangle.X + rectangle.Width, rectangle.Y);
        var cord3 = new Vector2(rectangle.X + rectangle.Width, rectangle.Y + rectangle.Height);
        var cord4 = new Vector2(rectangle.X, rectangle.Y + rectangle.Height);

        dist = Vector2.Distance(cord1, cord2);
        var dist2 = Vector2.Distance(cord2, cord3);
        var dist3 = Vector2.Distance(cord3, cord4);
        var dist4 = Vector2.Distance(cord1, cord4);

        angle = (float)Math.Atan2(cord2.Y - cord1.Y, cord2.X - cord1.X);
        var angle2 = (float)Math.Atan2(cord3.Y - cord2.Y, cord3.X - cord2.X);
        var angle3 = (float)Math.Atan2(cord4.Y - cord3.Y, cord4.X - cord3.X);
        var angle4 = (float)Math.Atan2(cord4.Y - cord1.Y, cord4.X - cord1.X);

        scale = new Vector2(dist, thickness);
        var scale2 = new Vector2(dist2, thickness);
        var scale3 = new Vector2(dist3, thickness);
        var scale4 = new Vector2(dist4, thickness);

        tex = new Texture2D(_spriteBatch.GraphicsDevice, 1, 1);
        tex.SetData(new[] { color });
        // Draw all 4 lines to make the rectangle
        _spriteBatch.Draw(tex, cord1, null, color, angle, Vector2.Zero,
            scale, SpriteEffects.None, 0);
        _spriteBatch.Draw(tex, cord2, null, color, angle2, Vector2.Zero,
            scale2, SpriteEffects.None, 0);
        _spriteBatch.Draw(tex, cord3, null, color, angle3, Vector2.Zero,
            scale3, SpriteEffects.None, 0);
        _spriteBatch.Draw(tex, cord1, null, color, angle4, Vector2.Zero,
            scale4, SpriteEffects.None, 0);
    }

    public void DrawLine()
    {
        _spriteBatch.Draw(tex, cord1, null, color, angle, Vector2.Zero,
            scale, SpriteEffects.None, 0);
    }
}